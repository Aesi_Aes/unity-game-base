﻿using UnityEngine;

namespace AndroidExtensions 
{
	public class AndroidImmersive
	{
		public static void EnableImmersiveMode()
		{
			#if UNITY_ANDROID && !UNITY_EDITOR
			AndroidJavaClass testClass = new AndroidJavaClass("com.exozet.game.immersivemode.ImmersiveModeEnabler");
			testClass.CallStatic("EnableImmersiveMode");
			#endif
		}
		
		public static void DisableImmersiveMode()
		{
			#if UNITY_ANDROID && !UNITY_EDITOR
			AndroidJavaClass testClass = new AndroidJavaClass("com.exozet.game.immersivemode.ImmersiveModeEnabler");
			testClass.CallStatic("DisableImmersiveMode");
			#endif
		}
	}
}