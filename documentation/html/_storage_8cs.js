var _storage_8cs =
[
    [ "Storage", "class_i_o_bridge_1_1_storage.html", null ],
    [ "BridgeConfiguration", "_storage_8cs.html#a68c4afc8aa92bae8fd06ac622080637d", [
      [ "useLocalStorage", "_storage_8cs.html#a68c4afc8aa92bae8fd06ac622080637da1728b538d4cae6d909013ca7339de02e", null ],
      [ "useRoamingStorage", "_storage_8cs.html#a68c4afc8aa92bae8fd06ac622080637daa4109610d4339823bc35c8669386e240", null ]
    ] ]
];