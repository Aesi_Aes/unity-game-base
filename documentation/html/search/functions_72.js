var searchData=
[
  ['randomizelist',['RandomizeList',['../class_helpers.html#af8e9c5939f62e8ff07f219a25e39e6c9',1,'Helpers']]],
  ['removeeventlistener',['RemoveEventListener',['../class_game_event_manager.html#a39a50956e449a95f69ae0bf327dc7898',1,'GameEventManager']]],
  ['removesavegameentry',['RemoveSaveGameEntry',['../class_game_save_games.html#a25def30896b0ca2400fbf06edb685526',1,'GameSaveGames']]],
  ['renderdescription',['RenderDescription',['../class_field_renderer_base.html#af68e52027cad93fa3897436667df0853',1,'FieldRendererBase']]],
  ['renderdescriptionicon',['RenderDescriptionIcon',['../class_field_renderer_base.html#a8472702901268c55a80fd9d5faee837a',1,'FieldRendererBase']]],
  ['restart',['Restart',['../class_game.html#abfd9525611737e68cf8be70c53705533',1,'Game']]],
  ['returnobjectinstance',['ReturnObjectInstance',['../class_u_g_b_object_pool.html#a374b11aab94ae91ab1a289feabfca06c',1,'UGBObjectPool']]]
];
