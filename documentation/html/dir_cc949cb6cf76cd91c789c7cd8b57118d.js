var dir_cc949cb6cf76cd91c789c7cd8b57118d =
[
    [ "CPinchGesture.cs", "_c_pinch_gesture_8cs.html", [
      [ "CPinchGesture", "class_c_pinch_gesture.html", "class_c_pinch_gesture" ],
      [ "PinchGestureEvent", "class_pinch_gesture_event.html", "class_pinch_gesture_event" ]
    ] ],
    [ "GameInput.cs", "_game_input_8cs.html", [
      [ "GameInput", "class_game_input.html", "class_game_input" ]
    ] ],
    [ "InputDelegates.cs", "_input_delegates_8cs.html", [
      [ "InputDelegates", "class_input_delegates.html", "class_input_delegates" ]
    ] ],
    [ "KeyMapping.cs", "_key_mapping_8cs.html", [
      [ "KeyMapping", "class_key_mapping.html", "class_key_mapping" ]
    ] ],
    [ "TouchDetection.cs", "_touch_detection_8cs.html", [
      [ "TouchDetection", "class_touch_detection.html", "class_touch_detection" ]
    ] ],
    [ "TouchInformation.cs", "_touch_information_8cs.html", [
      [ "TouchInformation", "class_touch_information.html", "class_touch_information" ]
    ] ]
];