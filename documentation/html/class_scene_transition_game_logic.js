var class_scene_transition_game_logic =
[
    [ "GameSetupReady", "class_scene_transition_game_logic.html#a788711852ac4f2c532c2081f51b16e93", null ],
    [ "GameStateChanged", "class_scene_transition_game_logic.html#a8e209fb326c0dd4c4448c0bd2c75b480", null ],
    [ "GetCurrentGameState", "class_scene_transition_game_logic.html#a3f3904cb50b2b5159b9b472d09278295", null ],
    [ "OnBeforePause", "class_scene_transition_game_logic.html#a521813d02bcaad7e3afa2579cfd04402", null ],
    [ "OnBeforeRestart", "class_scene_transition_game_logic.html#a9d424fb5fc4997f34a201548f85ebf2b", null ],
    [ "OnFirstTimeSaveGameLoaded", "class_scene_transition_game_logic.html#ac7c961861e5912f5a41b6d7439fdf121", null ],
    [ "OnSaveGameLoaded", "class_scene_transition_game_logic.html#a490b3a99916330ac6553171f8412b7f2", null ],
    [ "Start", "class_scene_transition_game_logic.html#ab89130b37a9806d421789e84898c1c6b", null ],
    [ "UpgradeSaveGameEntry", "class_scene_transition_game_logic.html#a77ec98936db5d33eee5d9d3c2774f06d", null ]
];