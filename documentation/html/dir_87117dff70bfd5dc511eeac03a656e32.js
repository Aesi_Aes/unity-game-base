var dir_87117dff70bfd5dc511eeac03a656e32 =
[
    [ "CLogCatcher.cs", "_c_log_catcher_8cs.html", [
      [ "CLogCatcher", "class_c_log_catcher.html", null ]
    ] ],
    [ "ELogLevel.cs", "_e_log_level_8cs.html", "_e_log_level_8cs" ],
    [ "ILogFormatter.cs", "_i_log_formatter_8cs.html", [
      [ "ILogFormatter", "interface_i_log_formatter.html", "interface_i_log_formatter" ]
    ] ],
    [ "ILogWriter.cs", "_i_log_writer_8cs.html", [
      [ "ILogWriter", "interface_i_log_writer.html", "interface_i_log_writer" ]
    ] ],
    [ "Logger.cs", "_logger_8cs.html", [
      [ "Logger", "class_logger.html", "class_logger" ]
    ] ]
];