var namespace_u_e_d_s =
[
    [ "SettingsProxy", "class_u_e_d_s_1_1_settings_proxy.html", "class_u_e_d_s_1_1_settings_proxy" ],
    [ "SettingsContainer", "class_u_e_d_s_1_1_settings_container.html", "class_u_e_d_s_1_1_settings_container" ],
    [ "ExistCheckParams", "class_u_e_d_s_1_1_exist_check_params.html", "class_u_e_d_s_1_1_exist_check_params" ],
    [ "DefaultIOProvider", "class_u_e_d_s_1_1_default_i_o_provider.html", "class_u_e_d_s_1_1_default_i_o_provider" ],
    [ "EditorSettingAttributeBase", "class_u_e_d_s_1_1_editor_setting_attribute_base.html", "class_u_e_d_s_1_1_editor_setting_attribute_base" ],
    [ "EditorSettingsContainerAttribute", "class_u_e_d_s_1_1_editor_settings_container_attribute.html", "class_u_e_d_s_1_1_editor_settings_container_attribute" ],
    [ "EditorSettingAttribute", "class_u_e_d_s_1_1_editor_setting_attribute.html", "class_u_e_d_s_1_1_editor_setting_attribute" ],
    [ "EditorSettingIOProviderAttribute", "class_u_e_d_s_1_1_editor_setting_i_o_provider_attribute.html", null ],
    [ "IGlobalEditorSettingsIOProvider", "interface_u_e_d_s_1_1_i_global_editor_settings_i_o_provider.html", "interface_u_e_d_s_1_1_i_global_editor_settings_i_o_provider" ],
    [ "Utils", "class_u_e_d_s_1_1_utils.html", null ],
    [ "ColorSerializer", "class_u_e_d_s_1_1_color_serializer.html", "class_u_e_d_s_1_1_color_serializer" ],
    [ "Vector3Serializer", "class_u_e_d_s_1_1_vector3_serializer.html", "class_u_e_d_s_1_1_vector3_serializer" ],
    [ "IPrimitveSerializer", "interface_u_e_d_s_1_1_i_primitve_serializer.html", "interface_u_e_d_s_1_1_i_primitve_serializer" ],
    [ "SerializedPrimitive", "class_u_e_d_s_1_1_serialized_primitive.html", "class_u_e_d_s_1_1_serialized_primitive" ],
    [ "SerializedValue", "class_u_e_d_s_1_1_serialized_value.html", "class_u_e_d_s_1_1_serialized_value" ],
    [ "Setting", "class_u_e_d_s_1_1_setting.html", "class_u_e_d_s_1_1_setting" ],
    [ "SerializedRoot", "class_u_e_d_s_1_1_serialized_root.html", "class_u_e_d_s_1_1_serialized_root" ],
    [ "SerializedSettingsContainer", "class_u_e_d_s_1_1_serialized_settings_container.html", "class_u_e_d_s_1_1_serialized_settings_container" ]
];