var class_game_input =
[
    [ "GetKeyMapping", "class_game_input.html#a6def59bc7211da2c029cc0dd10220948", null ],
    [ "GetTouch", "class_game_input.html#a44ed08b7462a9830e238b3513d07878b", null ],
    [ "OnDestroy", "class_game_input.html#a2241f05a66d1ac860a902aaf33ebfa63", null ],
    [ "Start", "class_game_input.html#ae22275364d818e46d3e7a77159b3bc4a", null ],
    [ "Update", "class_game_input.html#a14bc1f925f28db3d78a5b72e65b75175", null ],
    [ "kMappingGenericTap", "class_game_input.html#a696ce130de0638ac7c39fa984aa76050", null ],
    [ "mKeyMappings", "class_game_input.html#a3e9581dc28914e818b3ef4a3d6150d9d", null ],
    [ "OnKeyDown", "class_game_input.html#adc7fae880928e162da39219f45659a66", null ],
    [ "OnKeyMappingTriggered", "class_game_input.html#a95f1000816fc5754b0653c9d8d85e4e2", null ],
    [ "OnKeyUp", "class_game_input.html#abbab0b78a1f91f2f3b4acc051c81657b", null ]
];