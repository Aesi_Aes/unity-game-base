var dir_7a04eb71045862aaf771a3bcae56eab9 =
[
    [ "LocaData.cs", "_loca_data_8cs.html", [
      [ "LocaData", "class_loca_data.html", "class_loca_data" ]
    ] ],
    [ "SaveGameData.cs", "_save_game_data_8cs.html", [
      [ "SaveGameData", "class_serialization_1_1_save_game_data.html", "class_serialization_1_1_save_game_data" ],
      [ "XmlSaveGame", "class_serialization_1_1_save_game_data_1_1_xml_save_game.html", "class_serialization_1_1_save_game_data_1_1_xml_save_game" ],
      [ "XmlSaveGameEntry", "class_serialization_1_1_save_game_data_1_1_xml_save_game_entry.html", "class_serialization_1_1_save_game_data_1_1_xml_save_game_entry" ]
    ] ],
    [ "SaveGameEntry.cs", "_save_game_entry_8cs.html", [
      [ "SaveGameEntry", "class_serialization_1_1_save_game_entry.html", "class_serialization_1_1_save_game_entry" ]
    ] ],
    [ "XmlLocaData.cs", "_xml_loca_data_8cs.html", [
      [ "XmlLocaData", "class_xml_loca_data.html", "class_xml_loca_data" ],
      [ "XmlLocaDataEntry", "class_xml_loca_data_1_1_xml_loca_data_entry.html", "class_xml_loca_data_1_1_xml_loca_data_entry" ]
    ] ]
];