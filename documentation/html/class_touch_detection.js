var class_touch_detection =
[
    [ "GetTouch", "class_touch_detection.html#a885bbff67f8a467042ca8a5c192c4ee9", null ],
    [ "GetTouch", "class_touch_detection.html#a0ae00789f4ac28f8f4bc24ce120b8ccf", null ],
    [ "OnTouchEventDelegate", "class_touch_detection.html#aaf29a9b4994c981cd82dc68152efe736", null ],
    [ "Update", "class_touch_detection.html#a95d1677f775e9f7a320995903b3bc6b4", null ],
    [ "UpdateMouse", "class_touch_detection.html#a76a2816e80be52bb019e7ac6abea401e", null ],
    [ "UpdateTouch", "class_touch_detection.html#abb8e95e0d225209b73711ad999059e33", null ],
    [ "mTouches", "class_touch_detection.html#a35e03eea027485529bf05376f80c1557", null ],
    [ "currentTouchCnt", "class_touch_detection.html#a295303ff48a56c2b1c1d5848da755953", null ],
    [ "OnSwipeEvent", "class_touch_detection.html#ace00f4f29c469c804dbe9c4f9b7b4b42", null ],
    [ "OnTapEvent", "class_touch_detection.html#a1fa74ccaf9d64dabf7aeb5eb9a7a7b98", null ],
    [ "OnTouchEnd", "class_touch_detection.html#a195e62725a0abb03bd4517927a3cf47e", null ],
    [ "OnTouchStart", "class_touch_detection.html#afabf3f118a6fb826bf1cd3aea6c086c1", null ],
    [ "OnTouchUpdate", "class_touch_detection.html#a836a047b1724f1301a5feacb41f21e58", null ]
];