var class_u_e_d_s_1_1_default_i_o_provider =
[
    [ "DefaultIOProvider", "class_u_e_d_s_1_1_default_i_o_provider.html#a6b49c1174201a3e58e59d1b944814bb6", null ],
    [ "CreateEmpty< T >", "class_u_e_d_s_1_1_default_i_o_provider.html#ae38158f86e3e33227489ab4ff5969392", null ],
    [ "Exists", "class_u_e_d_s_1_1_default_i_o_provider.html#a229caf0d848dc6abf9b0b92663841ebc", null ],
    [ "GetData< T >", "class_u_e_d_s_1_1_default_i_o_provider.html#af6e212d0035af25873e153735a479d08", null ],
    [ "Load< T >", "class_u_e_d_s_1_1_default_i_o_provider.html#a895807da2beba69735367e3f7b29f0b3", null ],
    [ "Save< T >", "class_u_e_d_s_1_1_default_i_o_provider.html#a954e104b93a400f457703c81ba0b0011", null ],
    [ "LoaderError", "class_u_e_d_s_1_1_default_i_o_provider.html#a1e6c1ddfc20987576c5f5c6833544a2b", null ],
    [ "LoaderFinished", "class_u_e_d_s_1_1_default_i_o_provider.html#abadb038b571c5d36603117e61a1ee60f", null ],
    [ "LoaderHasError", "class_u_e_d_s_1_1_default_i_o_provider.html#a750a86530ee053ac657c053036f77350", null ],
    [ "WriterError", "class_u_e_d_s_1_1_default_i_o_provider.html#ad1aa03ccc4ad7db87baabcf49d46758c", null ],
    [ "WriterFinished", "class_u_e_d_s_1_1_default_i_o_provider.html#ad3d4002a208b160ae43e456c143d0fff", null ],
    [ "WriterHasError", "class_u_e_d_s_1_1_default_i_o_provider.html#ab4d5d2504589d15972698f537393e0b1", null ]
];