var dir_64893672f36dbd6c15aed645b3376a4e =
[
    [ "Misc", "dir_5287df7199790eb3c2e3410d0faf4ff3.html", "dir_5287df7199790eb3c2e3410d0faf4ff3" ],
    [ "PrimitveSerializers", "dir_977954c61e7dbc6162d8c1e7d00b370d.html", "dir_977954c61e7dbc6162d8c1e7d00b370d" ],
    [ "DefaultIOProvider.cs", "_default_i_o_provider_8cs.html", [
      [ "ExistCheckParams", "class_u_e_d_s_1_1_exist_check_params.html", "class_u_e_d_s_1_1_exist_check_params" ],
      [ "DefaultIOProvider", "class_u_e_d_s_1_1_default_i_o_provider.html", "class_u_e_d_s_1_1_default_i_o_provider" ]
    ] ],
    [ "EditorSettingAttributeBase.cs", "_editor_setting_attribute_base_8cs.html", [
      [ "EditorSettingAttributeBase", "class_u_e_d_s_1_1_editor_setting_attribute_base.html", "class_u_e_d_s_1_1_editor_setting_attribute_base" ],
      [ "EditorSettingsContainerAttribute", "class_u_e_d_s_1_1_editor_settings_container_attribute.html", "class_u_e_d_s_1_1_editor_settings_container_attribute" ],
      [ "EditorSettingAttribute", "class_u_e_d_s_1_1_editor_setting_attribute.html", "class_u_e_d_s_1_1_editor_setting_attribute" ],
      [ "EditorSettingIOProviderAttribute", "class_u_e_d_s_1_1_editor_setting_i_o_provider_attribute.html", null ]
    ] ],
    [ "IGlobalEditorSettingsIOProvider.cs", "_i_global_editor_settings_i_o_provider_8cs.html", [
      [ "IGlobalEditorSettingsIOProvider", "interface_u_e_d_s_1_1_i_global_editor_settings_i_o_provider.html", "interface_u_e_d_s_1_1_i_global_editor_settings_i_o_provider" ]
    ] ],
    [ "SerializedValue.cs", "_serialized_value_8cs.html", [
      [ "IPrimitveSerializer", "interface_u_e_d_s_1_1_i_primitve_serializer.html", "interface_u_e_d_s_1_1_i_primitve_serializer" ],
      [ "SerializedPrimitive", "class_u_e_d_s_1_1_serialized_primitive.html", "class_u_e_d_s_1_1_serialized_primitive" ],
      [ "SerializedValue", "class_u_e_d_s_1_1_serialized_value.html", "class_u_e_d_s_1_1_serialized_value" ]
    ] ],
    [ "Setting.cs", "_setting_8cs.html", [
      [ "Setting", "class_u_e_d_s_1_1_setting.html", "class_u_e_d_s_1_1_setting" ]
    ] ],
    [ "Settings.cs", "_settings_8cs.html", [
      [ "SerializedRoot", "class_u_e_d_s_1_1_serialized_root.html", "class_u_e_d_s_1_1_serialized_root" ],
      [ "SerializedSettingsContainer", "class_u_e_d_s_1_1_serialized_settings_container.html", "class_u_e_d_s_1_1_serialized_settings_container" ]
    ] ]
];