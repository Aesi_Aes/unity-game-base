var dir_d7bc7afca37e4d73dfa55c40d8d79bc3 =
[
    [ "Input", "dir_cc949cb6cf76cd91c789c7cd8b57118d.html", "dir_cc949cb6cf76cd91c789c7cd8b57118d" ],
    [ "Licensing", "dir_60b1f0aea83214be1e54933f20f7b3cd.html", "dir_60b1f0aea83214be1e54933f20f7b3cd" ],
    [ "CLocalizableText.cs", "_c_localizable_text_8cs.html", [
      [ "LocalizableText", "class_localizable_text.html", "class_localizable_text" ]
    ] ],
    [ "EGameMusicState.cs", "_e_game_music_state_8cs.html", "_e_game_music_state_8cs" ],
    [ "EGenericPlatform.cs", "_e_generic_platform_8cs.html", "_e_generic_platform_8cs" ],
    [ "FPSMeter.cs", "_f_p_s_meter_8cs.html", [
      [ "FPSMeter", "class_f_p_s_meter.html", null ]
    ] ],
    [ "Game.cs", "_game_8cs.html", [
      [ "Game", "class_game.html", "class_game" ]
    ] ],
    [ "GameComponent.cs", "_game_component_8cs.html", [
      [ "GameComponent", "class_game_component.html", "class_game_component" ]
    ] ],
    [ "GameData.cs", "_game_data_8cs.html", [
      [ "GameData", "class_game_data.html", null ]
    ] ],
    [ "GameLocalization.cs", "_game_localization_8cs.html", [
      [ "GameLocalization", "class_game_localization.html", "class_game_localization" ]
    ] ],
    [ "GameLogicImplementationBase.cs", "_game_logic_implementation_base_8cs.html", [
      [ "GameLogicImplementationBase", "class_game_logic_implementation_base.html", "class_game_logic_implementation_base" ]
    ] ],
    [ "GameMusic.cs", "_game_music_8cs.html", [
      [ "GameMusic", "class_game_music.html", "class_game_music" ]
    ] ],
    [ "GameOptions.cs", "_game_options_8cs.html", [
      [ "GameOptions", "class_game_options.html", "class_game_options" ]
    ] ],
    [ "GamePause.cs", "_game_pause_8cs.html", [
      [ "GamePause", "class_game_pause.html", null ]
    ] ],
    [ "GamePlayer.cs", "_game_player_8cs.html", [
      [ "GamePlayer", "class_game_player.html", "class_game_player" ]
    ] ],
    [ "GameSaveGames.cs", "_game_save_games_8cs.html", [
      [ "GameSaveGames", "class_game_save_games.html", "class_game_save_games" ]
    ] ],
    [ "GameStateManager.cs", "_game_state_manager_8cs.html", [
      [ "GameStateManager", "class_game_state_manager.html", "class_game_state_manager" ]
    ] ],
    [ "IPlayerController.cs", "_i_player_controller_8cs.html", [
      [ "IPlayerController", "interface_i_player_controller.html", "interface_i_player_controller" ]
    ] ],
    [ "LazyUpdates.cs", "_lazy_updates_8cs.html", [
      [ "LazyUpdates", "class_lazy_updates.html", "class_lazy_updates" ]
    ] ],
    [ "LogicDummy.cs", "_logic_dummy_8cs.html", [
      [ "LogicDummy", "class_dont_use_1_1_logic_dummy.html", "class_dont_use_1_1_logic_dummy" ]
    ] ],
    [ "LString.cs", "_l_string_8cs.html", [
      [ "LString", "class_l_string.html", "class_l_string" ]
    ] ],
    [ "SceneTransition.cs", "_scene_transition_8cs.html", [
      [ "SceneTransition", "class_scene_transition.html", "class_scene_transition" ]
    ] ],
    [ "SGameState.cs", "_s_game_state_8cs.html", [
      [ "SGameState", "struct_s_game_state.html", "struct_s_game_state" ]
    ] ],
    [ "SLanguages.cs", "_s_languages_8cs.html", [
      [ "SLanguages", "struct_s_languages.html", "struct_s_languages" ]
    ] ],
    [ "SMusicState.cs", "_s_music_state_8cs.html", [
      [ "SMusicState", "class_s_music_state.html", "class_s_music_state" ]
    ] ],
    [ "SPlayerState.cs", "_s_player_state_8cs.html", [
      [ "SPlayerState", "struct_s_player_state.html", "struct_s_player_state" ]
    ] ]
];