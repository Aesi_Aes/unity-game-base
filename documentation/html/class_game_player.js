var class_game_player =
[
    [ "OnPlayerStateChangedDelegate", "class_game_player.html#a1f3238a3c9f2e04b6042c1d6ba28c031", null ],
    [ "SetPlayerController", "class_game_player.html#ac3629b987e7cb25311b16a4a9ba79957", null ],
    [ "SetPlayerState", "class_game_player.html#a421a7a9f502c444cdb09a43b84ad6b9c", null ],
    [ "mPlayerPrefab", "class_game_player.html#a46574e4d7c255d853007e0aa8c7741c5", null ],
    [ "playerController", "class_game_player.html#a29ea98fc0c771f4bba74c26bf035124b", null ],
    [ "playerState", "class_game_player.html#af289a8d0c2c3e043d54d711e6d3f74fb", null ],
    [ "playerTransform", "class_game_player.html#ac51b466533d5fcb8e80d154d33294a9e", null ],
    [ "OnPlayerStateChanged", "class_game_player.html#aa78ce8c9d52dd2fd633acf8571cc8d14", null ]
];