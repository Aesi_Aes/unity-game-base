var class_key_mapping =
[
    [ "EKeyMode", "class_key_mapping.html#adbcd433f23a82342bfe3fdacfeba39e0", [
      [ "Any", "class_key_mapping.html#adbcd433f23a82342bfe3fdacfeba39e0aed36a1ef76a59ee3f15180e0441188ad", null ],
      [ "Up", "class_key_mapping.html#adbcd433f23a82342bfe3fdacfeba39e0a258f49887ef8d14ac268c92b02503aaa", null ],
      [ "Down", "class_key_mapping.html#adbcd433f23a82342bfe3fdacfeba39e0a08a38277b0309070706f6652eeae9a53", null ],
      [ "None", "class_key_mapping.html#adbcd433f23a82342bfe3fdacfeba39e0a6adf97f83acf6453d4a6a4b1070f3754", null ]
    ] ],
    [ "KeyMapping", "class_key_mapping.html#a5c8a1971546444b0a9d6c8be82d9e545", null ],
    [ "GetTouchId", "class_key_mapping.html#a86e2386d6e1b9a4286dbf9dc96d1d9b9", null ],
    [ "SetTouchId", "class_key_mapping.html#a8af433a0e8f28d85f091e72c4eb0e84b", null ],
    [ "mIsTap", "class_key_mapping.html#a9044f67638103f00bfac6335f61e449c", null ],
    [ "mKeyCode", "class_key_mapping.html#afaadfa57d1932d003e881f34ba1243b5", null ],
    [ "mKeyMode", "class_key_mapping.html#a79d6444a97d99a31ab94eb7b9e173b20", null ],
    [ "mName", "class_key_mapping.html#aaa38fe6069340c3588b07cd70eaf7dd8", null ],
    [ "mRelativeScreenRect", "class_key_mapping.html#ab986161ac58b3d8563a286c122befc55", null ],
    [ "mSwipeDirection", "class_key_mapping.html#a810d9da154dd1f834c9e1c0eb02606bd", null ]
];