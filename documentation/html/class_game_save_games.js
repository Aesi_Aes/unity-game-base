var class_game_save_games =
[
    [ "CreateNewSaveGame", "class_game_save_games.html#a475054bda8e302d1c450725279ebae97", null ],
    [ "GetSaveGameEntry", "class_game_save_games.html#ad18f2bc514cdb3d3cefd053778383134", null ],
    [ "LoadSaveGame", "class_game_save_games.html#ae40919c7e999a2bef6daf70ca99383a1", null ],
    [ "OnSavegameLoaded", "class_game_save_games.html#adf3402cc1a5d11d6808abeb1068f727d", null ],
    [ "OnSaveGameSelected", "class_game_save_games.html#a19cf70e9a31ab39f96ad6bf86a3c2fb6", null ],
    [ "RemoveSaveGameEntry", "class_game_save_games.html#a25def30896b0ca2400fbf06edb685526", null ],
    [ "SaveGame", "class_game_save_games.html#ad7fca7c8c7c6c43ccd7ff4995d6a5e76", null ],
    [ "mIsLoading", "class_game_save_games.html#a5a3020849c66dee2d3b89c626a9b26bb", null ],
    [ "currentSaveGame", "class_game_save_games.html#a94ed7cdc0b55f883cd5fa7e82ed80303", null ]
];