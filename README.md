Unity Game Base
==================

Goal
====

Unity Game Base (UGB) is a set of scripts and methods to create games using the Unity3D Game Engine in a defined and ordered manor. The idea of this project is to provide a handy set of tools to greatly improve the readability of Unity3D projects and speed up the development process.

The UGB is providing not only functionality, but a certain idea about how games should be set up in Unity3D. You should therefore consider adopting these ideas to get the best out of this project and the most use for your project.

Download
========

The latest version can be found here: https://bitbucket.org/kaiwegner/unity-game-base/downloads

You can find the release notes here: https://bitbucket.org/kaiwegner/unity-game-base/wiki/Release%20Notes

License
=======

Copyright (C) 2014 Kai Wegner
Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.