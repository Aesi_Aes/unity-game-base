package com.exozet.game.immersivemode;

import com.unity3d.player.UnityPlayer;

import android.annotation.TargetApi;
import android.app.Activity;
import android.os.Build;
import android.util.Log;
import android.view.View;

public class ImmersiveModeEnabler 
{
	
	@TargetApi(Build.VERSION_CODES.JELLY_BEAN)
	public static void EnableImmersiveMode(final Activity act)
	{
		if (Build.VERSION.SDK_INT >= 19) { //KITKAT
			act.runOnUiThread(new Runnable() {
				public void run() 
				{
					int flagImmersiveSticky = 4096; //View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
					int flags = flagImmersiveSticky
						| View.SYSTEM_UI_FLAG_FULLSCREEN
						| View.SYSTEM_UI_FLAG_HIDE_NAVIGATION;
			 
					View v = act.findViewById(android.R.id.content);
					v.setSystemUiVisibility(flags);
				}
			});
		}
		else
			Log.v("ImmersiveModeEnabler", "Android Version " + Build.VERSION.SDK_INT + " too low for immersive mode.");
	}
	
	@TargetApi(Build.VERSION_CODES.JELLY_BEAN)
	public static void DisableImmersiveMode(final Activity act)
	{
		if (Build.VERSION.SDK_INT >= 19) { //KITKAT
			act.runOnUiThread(new Runnable() {
				public void run() 
				{
					int flags = View.SYSTEM_UI_FLAG_VISIBLE;
			 
					View v = act.findViewById(android.R.id.content);
					v.setSystemUiVisibility(flags);
				}
			});
		}
		else
			Log.v("ImmersiveModeEnabler", "Android Version " + Build.VERSION.SDK_INT + " too low for immersive mode.");
	}
	
	@TargetApi(Build.VERSION_CODES.JELLY_BEAN)
	public static void EnableImmersiveMode()
	{
		EnableImmersiveMode(UnityPlayer.currentActivity);
	}
	
	@TargetApi(Build.VERSION_CODES.JELLY_BEAN)
	public static void DisableImmersiveMode()
	{
		DisableImmersiveMode(UnityPlayer.currentActivity);
	}
}
